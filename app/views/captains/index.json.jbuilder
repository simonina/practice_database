json.array!(@captains) do |captain|
  json.extract! captain, :id, :captainTeam, :captainID
  json.url captain_url(captain, format: :json)
end
