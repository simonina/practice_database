json.array!(@matches) do |match|
  json.extract! match, :id, :matchID, :time, :team, :against, :competition, :scoreFor, :scoreAgainst, :playerID1, :playerID2, :playerID3, :playerID4, :playerID5, :playerID6, :playerID7, :playerID8, :playerID9, :playerID10, :playerID11, :playerID12, :playerID13, :playerID14, :playerID15, :playerID16
  json.url match_url(match, format: :json)
end
