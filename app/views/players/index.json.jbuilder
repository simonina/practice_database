json.array!(@players) do |player|
  json.extract! player, :id, :playerID, :name, :team, :email, :mobile, :availabilityNotes, :unavailableFrom, :unavailableUntil
  json.url player_url(player, format: :json)
end
