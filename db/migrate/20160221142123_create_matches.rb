class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :matchID
      t.string :time
      t.integer :team
      t.string :against
      t.string :competition
      t.integer :scoreFor
      t.integer :scoreAgainst
      t.integer :playerID1
      t.integer :playerID2
      t.integer :playerID3
      t.integer :playerID4
      t.integer :playerID5
      t.integer :playerID6
      t.integer :playerID7
      t.integer :playerID8
      t.integer :playerID9
      t.integer :playerID10
      t.integer :playerID11
      t.integer :playerID12
      t.integer :playerID13
      t.integer :playerID14
      t.integer :playerID15
      t.integer :playerID16

      t.timestamps null: false
    end
  end
end
