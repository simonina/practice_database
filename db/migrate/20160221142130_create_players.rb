class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :playerID
      t.string :name
      t.integer :team
      t.string :email
      t.integer :mobile
      t.string :availabilityNotes
      t.string :unavailableFrom
      t.string :unavailableUntil

      t.timestamps null: false
    end
  end
end
