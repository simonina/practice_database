class CreateCaptains < ActiveRecord::Migration
  def change
    create_table :captains do |t|
      t.integer :captainTeam
      t.integer :captainID

      t.timestamps null: false
    end
  end
end
