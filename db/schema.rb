# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160221161237) do

  create_table "captains", force: :cascade do |t|
    t.integer  "captainTeam"
    t.integer  "captainID"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "matches", force: :cascade do |t|
    t.integer  "matchID"
    t.string   "time"
    t.integer  "team"
    t.string   "against"
    t.string   "competition"
    t.integer  "scoreFor"
    t.integer  "scoreAgainst"
    t.integer  "playerID1"
    t.integer  "playerID2"
    t.integer  "playerID3"
    t.integer  "playerID4"
    t.integer  "playerID5"
    t.integer  "playerID6"
    t.integer  "playerID7"
    t.integer  "playerID8"
    t.integer  "playerID9"
    t.integer  "playerID10"
    t.integer  "playerID11"
    t.integer  "playerID12"
    t.integer  "playerID13"
    t.integer  "playerID14"
    t.integer  "playerID15"
    t.integer  "playerID16"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "players", force: :cascade do |t|
    t.integer  "playerID"
    t.string   "name"
    t.integer  "team"
    t.string   "email"
    t.integer  "mobile"
    t.string   "availabilityNotes"
    t.string   "unavailableFrom"
    t.string   "unavailableUntil"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
