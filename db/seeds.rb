# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Match.create(
    matchID:1021015, 
    time:"12.45", 
    team:1, 
    against:"Weston",
    competition: "Div 1",
    scoreFor: 3,
    scoreAgainst: 1,
    playerID1: 12309280,
    playerID2: 12309281,
    playerID3: 12309282,
    playerID4: 12309283,
    playerID5: 12309284,
    playerID6: 12309285,
    playerID7: 12309286,
    playerID8: 12309287,
    playerID9: 12309288,
    playerID10: 12309289,
    playerID11: 12309290,
    playerID12: 12309291,
    playerID13: 12309292,
    playerID14: 12309293,
    playerID15: 12309294,
    playerID16: 12309295
    )
    
Player.create(
    playerID:12309280, 
    name:"Alex Simonin", 
    team:1, 
    email:"as@tcd.ie", 
    mobile:1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
    )
    
Player.create(
    playerID: 12309281, 
    name:"Tom Gibbs", 
    team: 1, 
    email:"tg@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309282, 
    name:"Ollie Welaratne", 
    team: 1, 
    email:"ow@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309283, 
    name:"David Stead", 
    team: 1, 
    email:"ds@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309284, 
    name:"Owen Monagon", 
    team: 1, 
    email:"om@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309285, 
    name:"Oisin Tiernan", 
    team: 1, 
    email:"ot@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309286, 
    name:"Hugh Lavery", 
    team: 1, 
    email:"hl@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309287, 
    name:"Aran Rooney", 
    team: 1, 
    email:"ar@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309288, 
    name:"Nick Welarante", 
    team: 1, 
    email:"nw@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309289, 
    name:"Robbie Clarke", 
    team: 1, 
    email:"rc@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309290, 
    name:"Johnny Lewis", 
    team: 1, 
    email:"jl@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309291, 
    name:"Wilf King", 
    team: 1, 
    email:"wk@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309292, 
    name:"Cillian Hynes", 
    team: 1, 
    email:"ch@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309293, 
    name:"Rory Nichols", 
    team: 1, 
    email:"rn@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309294, 
    name:"Ian Marron", 
    team: 1, 
    email:"im@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Player.create(
    playerID: 12309295, 
    name:"Ben Arrowsmith", 
    team: 1, 
    email:"ba@tcd.ie", 
    mobile: 1234567890, 
    availabilityNotes:"", 
    unavailableFrom:"",
    unavailableUntil:""
)

Captain.create(captainID:12309280, captainTeam:1)

