require 'test_helper'

class MatchesControllerTest < ActionController::TestCase
  setup do
    @match = matches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:matches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create match" do
    assert_difference('Match.count') do
      post :create, match: { against: @match.against, competition: @match.competition, matchID: @match.matchID, playerID10: @match.playerID10, playerID11: @match.playerID11, playerID12: @match.playerID12, playerID13: @match.playerID13, playerID14: @match.playerID14, playerID15: @match.playerID15, playerID16: @match.playerID16, playerID1: @match.playerID1, playerID2: @match.playerID2, playerID3: @match.playerID3, playerID4: @match.playerID4, playerID5: @match.playerID5, playerID6: @match.playerID6, playerID7: @match.playerID7, playerID8: @match.playerID8, playerID9: @match.playerID9, scoreAgainst: @match.scoreAgainst, scoreFor: @match.scoreFor, team: @match.team, time: @match.time }
    end

    assert_redirected_to match_path(assigns(:match))
  end

  test "should show match" do
    get :show, id: @match
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @match
    assert_response :success
  end

  test "should update match" do
    patch :update, id: @match, match: { against: @match.against, competition: @match.competition, matchID: @match.matchID, playerID10: @match.playerID10, playerID11: @match.playerID11, playerID12: @match.playerID12, playerID13: @match.playerID13, playerID14: @match.playerID14, playerID15: @match.playerID15, playerID16: @match.playerID16, playerID1: @match.playerID1, playerID2: @match.playerID2, playerID3: @match.playerID3, playerID4: @match.playerID4, playerID5: @match.playerID5, playerID6: @match.playerID6, playerID7: @match.playerID7, playerID8: @match.playerID8, playerID9: @match.playerID9, scoreAgainst: @match.scoreAgainst, scoreFor: @match.scoreFor, team: @match.team, time: @match.time }
    assert_redirected_to match_path(assigns(:match))
  end

  test "should destroy match" do
    assert_difference('Match.count', -1) do
      delete :destroy, id: @match
    end

    assert_redirected_to matches_path
  end
end
